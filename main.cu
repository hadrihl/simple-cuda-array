/*
*	File: main.cu
*	Description: Simple array CUDA
*	Author: hadrihl <hadrihilmi@gmail.com>
*/

#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

#define N 10

// cuda kernel
__global__ void mykernel(int* d_array) {
	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	d_array[idx] += 1;
}

// main
int main(int argc, char* argv[]) {

	// define var
	int* h_array;
	int* d_array;

	// init var
	h_array = (int*) calloc(N, sizeof(int));
	cudaMalloc((void**) &d_array, N * sizeof(int));

	// put dummy value
	int i;
	for(i = 0; i < N; i++) h_array[i] = i;

	// verify h_array
	for(i = 0; i < N; i++) printf("h_array[%d] = %d\n", i, h_array[i]);

	printf("\n\n");

	// cudacopy host-to-device
	cudaMemcpy(d_array, h_array, N * sizeof(int), cudaMemcpyHostToDevice);

	// launch kernel
	mykernel<<<1, 10>>>(d_array);

	// cudacopy device-to-host
	cudaMemcpy(h_array, d_array, N * sizeof(int), cudaMemcpyDeviceToHost);

	// verify h_array
	for(i = 0; i < N; i++) printf("h_array[%d] = %d\n", i, h_array[i]);

	// clean up
	cudaFree(d_array); free(h_array);

	return 0;
}
